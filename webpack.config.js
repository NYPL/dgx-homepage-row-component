const path = require('path');
const webpack = require('webpack');
const CleanBuild = require('clean-webpack-plugin');
const rootPath = path.resolve(__dirname);

if (process.env.NODE_ENV !== 'development') {
  module.exports = {
    devtool: 'source-map',
    entry: {
      'dgx-homepage-row-component': [
        path.resolve(rootPath, 'src/component.jsx'),
      ],
    },
    resolve: {
      extensions: ['', '.js', '.jsx'],
    },
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'index.min.js',
      libraryTarget: 'umd',
      library: 'HomepageRow',
    },
    externals: {
      // Required in order to ignore library within other components
      react: {
        root: 'React',
        commonjs2: 'react',
        commonjs: 'react',
        amd: 'react',
      },
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel',
          query: {
            presets: ['react', 'es2015'],
          },
        },
      ],
    },
    plugins: [
      new CleanBuild(['dist']),
      new webpack.optimize.UglifyJsPlugin({
        output: {
          comments: false,
        },
        compress: {
          warnings: true,
        },
      }),
    ],
  };
} else {
  module.exports = {
    devtool: 'eval',
    entry: [
      'webpack-dev-server/client?http://localhost:3000',
      'webpack/hot/only-dev-server',
      './src/app.jsx',
    ],
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'index.min.js',
      publicPath: '/',
    },
    plugins: [
      new CleanBuild(['dist']),
      new webpack.HotModuleReplacementPlugin(),
    ],
    resolve: {
      extensions: ['', '.js', '.jsx'],
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel',
          query: {
            presets: ['react', 'es2015'],
          },
        },
      ],
    },
  };
}
