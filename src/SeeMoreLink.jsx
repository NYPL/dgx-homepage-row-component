// Import libraires
import React from 'react';
import PropTypes from 'prop-types';
// Import components
import { DotsIcon } from '@nypl/dgx-svg-icons';

const SeeMoreLink = ({
  id,
  target,
  label,
  className,
  gaClickEvent,
}) => (
  <a
    id={id}
    className={`seeMoreLink ${className} svgIcon`}
    href={target}
    onClick={gaClickEvent ? () => gaClickEvent('See More') : null}
  >
    <DotsIcon ariaHidden />
    <span className="label">
      {label}
    </span>
  </a>
);

SeeMoreLink.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  label: PropTypes.string,
  target: PropTypes.string,
  style: PropTypes.object,
  gaClickEvent: PropTypes.func,
};

export default SeeMoreLink;
