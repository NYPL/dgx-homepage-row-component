// Import libraires
import React from 'react';
import { render } from 'react-dom';

// Import components
import HomepageRow from './component.jsx';
import FeatureRow from 'dgx-feature-row-component';

const title = {
  en: {
    text: 'Homepage Row Title',
  },
};

// Passes the label as a function param. The action is determined in the component to
// use "Heading" or "See More".
const gaClickTest = (label) => (
  (action) => {
    console.log(action);
    console.log(label);
  }
);

/* app.jsx
 * Used for local development of React Components
 */
render(
  <HomepageRow
    title={title}
    link="https://encrypted.google.com/search?q=good+data"
    gaClickEvent={gaClickTest('What\'s Happening')}
    content={
      <FeatureRow
        name="hpLearnSomethingNew"
        id="hpLearnSomethingNew"
        items={[]}
      />
    }
  />, document.getElementById('hpRowComponent')
);
