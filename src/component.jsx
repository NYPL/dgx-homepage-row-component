// Import libraires
import React from 'react';
import PropTypes from 'prop-types';
// Import components
import SeeMoreLink from './SeeMoreLink';

const HomepageRow = ({
  className,
  lang,
  link,
  title,
  content,
  seeMoreId,
  seeMoreStyle,
  gaClickEvent,
}) => {
  /**
    * @desc Verifies that the element exists with the default lang and returns the proper value.
    * @param {Object} param - Element containing object with language property.
    * @param {String} param - String representation of the language property name.
    * @returns {String} param - Returns the string value for the given property match.
    */
  const getLangText = (elem = {}, language) => (
    (elem && elem[language] && elem[language].text) ? elem[language].text : null
  );

  /**
    * @desc Returns the DOM element H2 holding the title string.
    * @returns {Object} param - Returns the React H2 DOM element with proper string.
    */
  const renderRowTitle = () => {
    const rowTitle = getLangText(title, lang);
    if (rowTitle) {
      if (link) {
        return (
          <h2 className="title">
            <a href={link} onClick={gaClickEvent ? () => gaClickEvent('Heading') : null}>
              {rowTitle}
            </a>
          </h2>
        );
      }
      return <h2 className="title">{rowTitle}</h2>;
    }
    return null;
  };

  return (
    <div className={className}>
      <div className="leftColumn">
        {renderRowTitle()}
      </div>
      <div className="rightColumn">
        {content}
      </div>
      <div className="seeMoreWrapper">
        {(title && link) ?
          <SeeMoreLink
            id={seeMoreId}
            target={link}
            label="SEE MORE"
            style={seeMoreStyle}
            gaClickEvent={gaClickEvent}
          /> : null
        }
      </div>
    </div>
  );
};

HomepageRow.propTypes = {
  className: PropTypes.string,
  title: PropTypes.object,
  link: PropTypes.string,
  content: PropTypes.object,
  lang: PropTypes.string,
  seeMoreId: PropTypes.string,
  seeMoreStyle: PropTypes.object,
  gaClickEvent: PropTypes.func,
};

HomepageRow.defaultProps = {
  className: 'hpRow',
  lang: 'en',
};

export default HomepageRow;
