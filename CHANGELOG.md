## Changelog

### v0.3.0
- Updating SVG icons to @nypl/dgx-svg-icons.

### v0.2.0
- Upgraded to React 15.

### v0.1.0
#### Added
- Added support for Google Analytics click events via `gaClickEvent` function property.

#### Changed
- Enhanced the README document by adding a Changelog and Version.
