# Homepage Row Component

This component is used to be the general container for the feature components of dgx-homepage app.

## Version
> v0.3.0

## Usage

> Require `dgx-homepage-row-component` as a dependency in your `package.json` file.

```sh
"dgx-homepage-row-component": "git+ssh://git@bitbucket.org/NYPL/dgx-homepage-row-component.git#master"
```

> Once installed, import the component in your React Application.

```sh
import HomepageRow from 'dgx-homepage-row-component';
```

## Props

> You may initialize the component with the following properties:

```sh
<HomepageRow
  className="hpRow" ## String
  title="HP-What\'s Happening" ## String
  link="//domain.org" ## String
  content={object} ## Contains component content
  seeMoreStyle={object} ## Contains style object for link
  seeMoreId="seeMore" ## String ID assignment for link
  gaClickEvent={gaClickFunc("What\'s Happening")} ## Function for Google Analytics click events. Action is the parameter required, the label is determined by the component to use either 'Heading' or 'See More'.
/>
```

- `className`:  Value used as the "element" in a BEM naming scheme (default: `hpRow`)
- `title`: Value to use as the title of the feature component
- `link`: URL leads to the feature component's landing page which HomepageRow contains (default: `#`)
- `seeMoreId`: String value to used on the seeMoreLink.
- `seeMoreStyle`: Object that contains style attributes for the seeMoreLink
- `content`: Array data to be passed to the feature component's right column
- `gaClickEvent`: Function used to track Google Analytics click events on the Heading and SeeMore link
- `lang`: Language to display (default: `en`)
